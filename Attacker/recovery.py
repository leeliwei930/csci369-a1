#!/usr/bin/env python3
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
from base64 import b64encode, b64decode
import sys




class RSADecrypter:
    privateKey = ""
    privateKeyFileName = None
    cipherRSA = None
    def __init__(self, privateKeyFileName):
        self.privateKeyFileName = privateKeyFileName

    def loadPrivateKey(self):
        try:
            privateKeyFile = open(self.privateKeyFileName , "rb")
            self.privateKey = RSA.import_key(privateKeyFile.read())
            self.cipherRSA = PKCS1_OAEP.new(self.privateKey)
            privateKeyFile.close()
        except FileNotFoundError as notfound:
            print("Unable to load the private key file name " + notfound.filename)
            exit()
            
        
    
    def decrypt(self, encryptedContent):
        return self.cipherRSA.decrypt(encryptedContent)

    def validate(self):
        return self.privateKeyFileName != None and len(self.privateKeyFileName) > 0
def boot():
    if(len(sys.argv) >=3):
        privateKeyFileName = sys.argv[1]
        ransomKeyFileName = sys.argv[2]
    else:
        privateKeyFileName = input("Please enter the private key filename: ")
        ransomKeyFileName = input("Please enter the encrypted symmetric filename: ")
    
    rsaDecrypter = RSADecrypter(privateKeyFileName)
    rsaDecrypter.loadPrivateKey()

    
    try:
        ransomKeyFile = open(ransomKeyFileName, "rb")
        ransomKeyContent = ransomKeyFile.read()

        decryptedContent = rsaDecrypter.decrypt(b64decode(ransomKeyContent))

        decryptedRansomKeyFileName = ransomKeyFileName.replace(".bin", ".txt")
        decryptedRansomKey = open(decryptedRansomKeyFileName, "w")
        decryptedRansomKey.write(b64encode(decryptedContent).decode('utf-8'))
        decryptedRansomKey.close()
        ransomKeyFile.close()

    except FileNotFoundError as notfound:
        print("Unable to locate the file name as " + notfound.filename)

    
boot()
