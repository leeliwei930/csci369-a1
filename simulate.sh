#!/bin/bash

echo "Switching to Victim Role ..."

cd ./Victim

echo "Victim create text file"
touch "Hello.txt"
echo "Hello Word" > "Hello.txt"

echo "Victim runs the ransomware"
python3 crack.py

echo "Victim machine file get encypted"

cat "*.enc"

echo "Victim pays for ransom"

echo "Victim send encryption key to attacker"

cp ./ransomkey.bin ../Attacker

echo "Attacker decrypt the key"

cd ../Attacker

python3 recovery.py private.pem ransomkey.bin

echo "Attacker send back the encryption key"

mv ./ransomkey.txt ../Victim

echo "Switching to Victim Role"
cd ../Victim

python3 decrypt.py ransomkey.txt

echo "Display recovered file"
cat *.txt