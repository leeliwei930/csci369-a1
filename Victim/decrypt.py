#!/usr/bin/env python3
import os;
import re;
import fnmatch
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Util.Padding import pad, unpad
from base64 import b64encode, b64decode
import sys


class FileDecrypter:
    base64SymmetricKey = ""
    encryptedFiles = list()
    b64InitialVector = "xP8f9OkXH+ApKnCbiDn9uA=="
    def __init__(self, encryptedFiles):
        self.encryptedFiles = encryptedFiles


    def validate(self):
        return  len(self.base64SymmetricKey) > 0


    def promptSymmetricKey(self):
        while(self.validate() == False):
            if len(sys.argv) >= 2:
                decryptedSymmetricFileName = sys.argv[1]
            else:
                decryptedSymmetricFileName = input("Please enter the decryption key file name: ")
                try:
                    symmetricKeyFile = open(decryptedSymmetricFileName, 'rb')
                    self.base64SymmetricKey = symmetricKeyFile.read()
                    symmetricKeyFile.close()
                except FileNotFoundError as notFound:
                    print("Unable to find the file the decryption key file name " + notFound.filename)
            
    def decrypt(self):
        currentDecryptionFileName = ""
        try:
            initialVector = b64decode(self.b64InitialVector)
            symmetricKey = b64decode(self.base64SymmetricKey)
            aesCipher = AES.new(symmetricKey, AES.MODE_CBC, initialVector)
            for encryptedFileName in self.encryptedFiles:
                decryptedFileName = encryptedFileName
                decryptedFileName = decryptedFileName.replace(".enc", ".txt")
                encryptedFileContent = open(encryptedFileName, "rb")
                contentBlock = aesCipher.decrypt(b64decode(encryptedFileContent.read()))
                decryptedContent = unpad(contentBlock, AES.block_size)
                decryptedFile = open(decryptedFileName, "wb")
                decryptedFile.write(decryptedContent)
                decryptedFile.close()
                encryptedFileContent.close()
                os.remove(encryptedFileName)
            print("All *.txt files are decrypted.")
        except ValueError:
            print("value error")
        except KeyError:
            print("key error")

        
class FileOperator:
    files = list()
    encryptedFiles = list()

    def __init__(self):
        self.files = os.listdir('.')

    def filterFilesBasedOnExtension(self, extension = "*.enc") :
        for filename in self.files:
            if fnmatch.fnmatch(filename, extension):
                self.encryptedFiles.append(filename)
def boot():
    fileOperator = FileOperator()
    fileOperator.filterFilesBasedOnExtension()

    encryptedFiles = fileOperator.encryptedFiles

    fileDecrypter = FileDecrypter(encryptedFiles)

    fileDecrypter.promptSymmetricKey()

    fileDecrypter.decrypt()

boot()